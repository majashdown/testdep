# testdep

## Environment variables

Set in the common CI file used by the projects:

  - `DEP_POLLING_INTERVAL`: Polling interval for pre script

Set by the pre script when triggering the pipeline in downstream projects. They
are used by the pre script in downstream projects to wait for the upstream
pipelines to finish and to download their artefacts:

  - `DEP_ORIGIN_SHA`: Git hash of the origin project; is is used by downstream
    projects to tag the builds
  - `DEP_UPSTREAM_PROJECTS`: Comma-separated list of upstream projects
  - `DEP_UPSTREAM_PIPELINES`: Comma-separated list of upstream pipeline IDs
