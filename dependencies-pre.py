import io
import os
import sys
import time
import base64
import urllib
import zipfile
import requests

# Parameters

api = 'https://gitlab.com/api/v4'
config_file = '.dependencies'
dep_dir = 'dep'
inp_dir = os.path.join(dep_dir, 'inputs')
out_dir = os.path.join(dep_dir, 'outputs')
ver_file = os.path.join(dep_dir, 'version')


# Functions to call GitLab API

def url_encode(input):
    """
    URL-encode a string.

    :param input: input string
    :returns: URL-encoded version of the input string

    """
    return urllib.parse.quote(input, safe='')


def get_project_info(project):
    """
    Get project default branch and list of its downstream projects.

    :param project: project
    :returns: branch name and list of downstream dependencies

    """
    project_enc = url_encode(project)

    url = f'{api}/projects/{project_enc}'
    res = requests.get(url)
    if not res.ok:
        print(f'Cannot get project {project}: {res.reason}')
        sys.exit(1)
    data = res.json()
    branch = data['default_branch']

    url = f'{api}/projects/{project_enc}/repository/files/{config_file}'
    params = {'ref': branch}
    res = requests.get(url, params=params)
    if not res.ok:
        print(f'Cannot get project {project} configuration file: {res.reason}')
        sys.exit(1)
    data = res.json()
    content = base64.b64decode(data['content']).decode()
    downstream = parse_downstream(content)

    return branch, downstream


def trigger_pipeline(project, branch, variables):
    """
    Trigger pipeline in project.

    :param project: name of project
    :param branch: name of branch
    :param variables: dict of variables to pass to pipeline

    """
    project_enc = url_encode(project)
    url = f'{api}/projects/{project_enc}/trigger/pipeline'
    data = {
        'ref': branch,
        'token': os.getenv('CI_JOB_TOKEN')
    }
    data.update({f'variables[{k}]': v for k, v in variables.items()})
    res = requests.post(url, data=data)
    if not res.ok:
        print(f'Cannot trigger pipeline in project {project}: {res.reason}')
        sys.exit(1)
    data = res.json()
    pipeline = str(data['id'])
    return pipeline


def get_pipeline_post_job(project, pipeline):
    """
    Get the ID of a pipeline post job.

    :param project: name of project
    :param pipeline: pipeline ID
    :returns: job ID

    """
    project_enc = url_encode(project)
    url = f'{api}/projects/{project_enc}/pipelines/{pipeline}/jobs'
    res = requests.get(url)
    if not res.ok:
        print(f'Cannot get jobs from project {project} pipeline {pipeline}: {res.reason}')
        sys.exit(1)
    data = res.json()

    job_id = None
    for job in data:
        if job['name'] == 'dependencies-post':
            job_id = str(job['id'])
            break
    if not job_id:
        print(f'Cannot find post job in project {project} pipeline {pipeline}')
        sys.exit(1)

    return job_id


def get_pipeline_status(project, pipeline):
    """
    Get the status of a pipeline.

    :param project: name of project
    :param pipeline: pipeline ID
    :returns: pipeline status

    """
    project_enc = url_encode(project)
    url = f'{api}/projects/{project_enc}/pipelines/{pipeline}'
    res = requests.get(url)
    if not res.ok:
        print(f'Cannot get status of project {project} pipeline {pipeline}: {res.reason}')
        sys.exit(1)
    data = res.json()
    status = data['status']
    return status


def get_job_artefacts(project, job):
    """
    Get artefacts from pipeline job.

    :param project: name of project
    :param job: job ID
    :returns: zip file containing artefacts

    """
    project_enc = url_encode(project)
    url = f'{api}/projects/{project_enc}/jobs/{job}/artifacts'
    res = requests.get(url)
    if not res.ok:
        print(f'Cannot get artefacts from project {project} job {job}: {res.reason}')
        sys.exit(1)
    return res.content


def parse_downstream(input):
    """
    Parse dependencies file.

    :param input: contents of dependencies file
    :returns: list of downstream dependencies

    """
    return input.splitlines()


def wait_for_upstream():
    """
    Wait for upstream pipelines to finish and download their artefacts.
    """
    # Unpack the data about the upstream pipelines

    projects = os.getenv('DEP_UPSTREAM_PROJECTS').split(',')
    pipelines = os.getenv('DEP_UPSTREAM_PIPELINES').split(',')

    upstream = []
    for project, pipeline in zip(projects, pipelines):
        post_job = get_pipeline_post_job(project, pipeline)
        print(f'Waiting for project {project} pipeline {pipeline}')
        upstream.append({
            'project': project,
            'pipeline': pipeline,
            'post_job': post_job,
            'status': None,
        })

    # Wait for upstream pipelines to finish

    polling_interval = float(os.getenv('DEP_POLLING_INTERVAL', '60'))

    while True:

        for u in upstream:
            if u['status'] != 'success':
                status = get_pipeline_status(u['project'], u['pipeline'])
                if status == 'success':
                    print(f'Project {u["project"]} pipeline {u["pipeline"]} has succeeded')
                elif status == 'failed':
                    print(f'Project {u["project"]} pipeline {u["pipeline"]} has failed')
                    sys.exit(1)
                u['status'] = status

        if all(project['status'] == 'success' for project in upstream):
            break

        print(f'Sleeping for {polling_interval} seconds')
        time.sleep(polling_interval)

    # Fetch artefacts

    print('Fetching artefacts')

    for u in upstream:
        print(f'Fetching artefacts from project {u["project"]}')
        artefacts_raw = get_job_artefacts(u['project'], u['post_job'])
        artefacts_zip = zipfile.ZipFile(io.BytesIO(artefacts_raw))
        artefacts_zip.extractall()

    # Move artefacts to inputs directory

    print('Artefacts:')

    for f in sorted(os.listdir(out_dir)):
        print(f)
        os.rename(os.path.join(out_dir, f), os.path.join(inp_dir, f))


def trigger_downstream():
    """
    Trigger the downstream dependencies of this project.
    """
    # Immediate downstream projects
    with open(config_file, 'r') as f:
        downstream = parse_downstream(f.read())

    if not downstream:
        print('No downstream dependencies')
        return

    # This project's full name with namespace
    project = os.getenv('CI_PROJECT_PATH')
    # This pipeline's ID
    pipeline = os.getenv('CI_PIPELINE_ID')

    # Add this project as the first node in the dependency graph
    dependencies = {
        project: {
            'branch': None, # Not needed
            'pipeline': pipeline,
            'upstream': [] # No upstream projects
        }
    }

    # Function to recursively construct graph
    def recurse(p, p_downstream):
        for d in p_downstream:
            if d not in dependencies:
                branch, d_downstream = get_project_info(d)
                dependencies[d] = {
                    'branch': branch,
                    'upstream': [p],
                    'pipeline': None,
                }
                recurse(d, d_downstream)
            else:
                dependencies[d]['upstream'].append(p)

    # Construct the graph
    recurse(project, downstream)

    # Loop until all the pipelines are triggered.
    while True:

        for project, info in dependencies.items():

            if info['pipeline'] is None:

                projects = info['upstream']
                pipelines = [dependencies[p]['pipeline'] for p in projects]

                if all(p is not None for p in pipelines):

                    variables = {
                        'DEP_ORIGIN_SHA': os.getenv('CI_COMMIT_SHA'),
                        'DEP_UPSTREAM_PROJECTS': ','.join(projects),
                        'DEP_UPSTREAM_PIPELINES': ','.join(pipelines)
                    }

                    print(f'Triggering pipeline in project {project}')
                    pipeline = trigger_pipeline(project, info['branch'], variables)
                    dependencies[project]['pipeline'] = pipeline

        if all(info['pipeline'] is not None for info in dependencies.values()):
            break


def main():
    """Main."""

    print('Dependencies pre script')

    # If configuration file is not present, exit

    if not os.path.exists(config_file):
        print('Configuration file not found: exiting')
        sys.exit()

    # Create directories for inputs and outputs

    print('Creating directories for inputs and outputs')
    os.makedirs(inp_dir)
    os.makedirs(out_dir)

    origin_sha = os.getenv('DEP_ORIGIN_SHA')

    if origin_sha:

        print('Pipeline was triggered by upstream pipeline')

        wait_for_upstream()

        # Get SHA for build

        sha = origin_sha

    else:

        print('Triggering downstream pipelines')

        trigger_downstream()

        # Get SHA for build

        commit_branch = os.getenv('CI_COMMIT_BRANCH')
        default_branch = os.getenv('CI_DEFAULT_BRANCH')

        if commit_branch == default_branch:
            sha = None
        else:
            sha = os.getenv('CI_COMMIT_SHA')

    # Set version for build (assuming input is in .version)

    with open('.version', 'r') as f:
        version = f.read().strip()

    if sha:
        short_sha = sha[0:8]
        version += '+' + short_sha

    print(f'Building version {version}')

    with open(ver_file, 'w') as f:
        print(version, file=f)


if __name__ == '__main__':
    main()
